<?php

namespace Multisanti\Vk\Exceptions;

use Multisanti\Vk\Requests\VkRequestInterface;

class RequestFailedException extends \Exception
{
    /** @var VkRequestInterface $request*/
    protected $request;
    protected $token = "";

    /**
     * @return VkRequestInterface
     */
    public function getRequest(): VkRequestInterface
    {
        return $this->request;
    }

    /**
     * @param VkRequestInterface $request
     */
    public function setRequest(VkRequestInterface $request): void
    {
        $this->request = $request;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }
}
