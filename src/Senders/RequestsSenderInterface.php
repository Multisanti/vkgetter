<?php

namespace Multisanti\Vk\Senders;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\VkRequestInterface;

interface RequestsSenderInterface
{
    /**
     * @param VkRequestInterface $vk_request
     * @param string $access_token
     * @return string
     * @throws RequestFailedException
     */
    public function send(VkRequestInterface $vk_request, string $access_token): string;
}