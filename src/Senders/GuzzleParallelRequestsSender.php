<?php

namespace Multisanti\Vk\Senders;

use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\ParallelVkRequestInterface;
use Multisanti\Vk\Results\ParallelResult;
use Multisanti\Vk\Results\ParallelResultsCollection;
use Multisanti\Vk\Results\ParallelResultsCollectionInterface;
use Multisanti\Vk\Senders\Options\ApiOptionsInterface;
use Multisanti\Vk\Senders\Options\ApiUrlInterface;

class GuzzleParallelRequestsSender implements ParallelRequestsSenderInterface
{
    private $guzzle;
    private $api_url;
    private $api_options;

    private $concurrency = 3;
    
    public function __construct(Client $guzzle, ApiUrlInterface $api_url, ApiOptionsInterface $api_options)
    {
        $this->guzzle = $guzzle;
        $this->api_url = $api_url;
        $this->api_options = $api_options;
    }

    /**
     * @param array $parallel_requests
     * @return array
     */
    public function send(array $parallel_requests): ParallelResultsCollectionInterface
    {
        $results_collection = new ParallelResultsCollection();
        $guzzle_requests = $this->convertToGuzzleRequests($parallel_requests);
        $pool_config = [
            'concurrency' => $this->concurrency,
            'fulfilled' => function (Response $response, $index) use ($parallel_requests, $results_collection) {
                /** @var ParallelVkRequestInterface $request */
                $request = $parallel_requests[$index];
                $result = new ParallelResult($request, $response->getBody()->getContents());
                $results_collection->addResult($result);
            },
            'rejected' => function ($reason, $index) use ($parallel_requests, $results_collection) {
                $message = "Unknown error";
                if ($reason instanceof \Throwable) {
                    $message = $reason->getMessage();
                }
                /** @var ParallelVkRequestInterface $request */
                $request = $parallel_requests[$index];
                $exception = new RequestFailedException($message);
                $exception->setRequest($request->getRequest());
                $exception->setToken($request->getToken());
                $results_collection->addException($exception);
            },
        ];
        $pool = new Pool($this->guzzle, $guzzle_requests, $pool_config);
        $promise = $pool->promise();
        $promise->wait();
        return $results_collection;
    }

    private function convertToGuzzleRequests(array $parallel_requests): array
    {
        $converted = [];
        foreach ($parallel_requests as $parallel_request) {
            if(!($parallel_request instanceof ParallelVkRequestInterface)) {
                throw new \InvalidArgumentException("only instances of ParallelVkRequestInterface accepted");
            }
            $method_name = $parallel_request->getRequest()->getMethodName();
            $method_params = $parallel_request->getRequest()->getMethodParams();
            $access_token = $parallel_request->getToken();
            $url = $this->api_url->getMethodUrl($method_name);
            $params = $this->api_options->merge($method_params, $access_token);
            $guzzle_request = new Request("post", $url, [], http_build_query($params));
            $converted[] = $guzzle_request;
        }
        return $converted;
    }

    /**
     * @return int
     */
    public function getConcurrency(): int
    {
        return $this->concurrency;
    }

    /**
     * @param int $concurrency
     */
    public function setConcurrency(int $concurrency): void
    {
        $this->concurrency = $concurrency;
    }
}
