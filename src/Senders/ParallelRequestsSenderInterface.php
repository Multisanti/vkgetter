<?php

namespace Multisanti\Vk\Senders;

use Multisanti\Vk\Requests\ParallelVkRequestInterface;
use Multisanti\Vk\Results\ParallelResultsCollectionInterface;

interface ParallelRequestsSenderInterface
{
    /**
     * @param ParallelVkRequestInterface[] $requests
     * @return array
     */
    public function send(array $requests): ParallelResultsCollectionInterface;
}