<?php

namespace Multisanti\Vk\Senders\Options;

interface ApiUrlInterface
{
    public function getMethodUrl(string $method_name): string;

}