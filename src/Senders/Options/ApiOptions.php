<?php

namespace Multisanti\Vk\Senders\Options;

class ApiOptions implements ApiOptionsInterface
{
    private $api_v = '5.92';
    private $lang = 'ru';

    public function __construct($api_v = '5.92', $lang = 'ru')
    {
        $this->setApiV($api_v);
        $this->setLang($lang);
    }

    public function get(): array
    {
        return [
            "v" => $this->getApiV(),
            "lang" => $this->getLang(),
        ];
    }

    public function merge(array $method_params, string $access_token): array
    {
        $merged = array_merge($method_params, $this->get());
        $merged["access_token"] = $access_token;
        return $merged;
    }


    /**
     * @return string
     */
    public function getApiV(): string
    {
        return $this->api_v;
    }

    /**
     * @param string $api_v
     */
    public function setApiV(string $api_v): void
    {
        $this->api_v = $api_v;
    }

    /**
     * @return string
     */
    public function getLang(): string
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang(string $lang): void
    {
        $this->lang = $lang;
    }



}