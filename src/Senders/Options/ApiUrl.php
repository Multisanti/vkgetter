<?php

namespace Multisanti\Vk\Senders\Options;

class ApiUrl implements ApiUrlInterface
{
    private $base_url;

    public function __construct(string $base_url = 'https://api.vk.com/method')
    {
        $this->base_url = $base_url;
    }

    public function getMethodUrl(string $method_name): string
    {
        return "{$this->base_url}/{$method_name}";
    }

}