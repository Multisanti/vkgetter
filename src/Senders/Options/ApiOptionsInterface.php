<?php

namespace Multisanti\Vk\Senders\Options;

interface ApiOptionsInterface
{
    public function get(): array;
    public function merge(array $method_params, string $access_token): array;
}