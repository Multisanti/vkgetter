<?php

namespace Multisanti\Vk\Senders\Decorators\Parallel;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\ParallelVkRequest;
use Multisanti\Vk\Requests\VkRequestInterface;
use Multisanti\Vk\Results\ParallelResult;
use Multisanti\Vk\Results\ParallelResultInterface;
use Multisanti\Vk\Results\ParallelResultsCollectionInterface;
use Multisanti\Vk\Senders\ParallelRequestsSenderInterface;
use Multisanti\Vk\Senders\RequestsSenderInterface;

class RetriesVkErrors implements ParallelRequestsSenderInterface
{
    private $sender;
    private $retrier;

    private $errors_to_retry = [
        6 => 'Too many requests per second',
        9 => 'Flood control',
        10 => 'Internal server error',
    ];

    public function __construct(ParallelRequestsSenderInterface $sender, RequestsSenderInterface $retrier)
    {
        $this->sender = $sender;
        $this->retrier = $retrier;
    }

    public function send(array $requests): ParallelResultsCollectionInterface
    {
        $parallel_results = $this->sender->send($requests);
        $rejected = [];
        $fulfilled = [];

        $results = $parallel_results->getResults();
        while (count($results) > 0) {
            /** @var ParallelResultInterface $parallel_result */
            $parallel_result = array_pop($results);
            $vk_request = $parallel_result->getRequest()->getRequest();
            $access_token = $parallel_result->getRequest()->getToken();
            $vk_response = $parallel_result->getResult();

            $decoded = json_decode($vk_response, true);
            if ($this->isValidResponse($decoded)) {
                $fulfilled[] = $parallel_result;
                continue;
            }
            if($this->isRetrieable($decoded)) {
                try {
                    $response = $this->retrier->send($vk_request, $access_token);
                    $parallel_request = new ParallelVkRequest($vk_request, $access_token);
                    $parallel_result = new ParallelResult($parallel_request, $response);
                    $fulfilled[] = $parallel_result;
                }
                catch (RequestFailedException $e) {
                    $rejected[] = $e;
                }
                continue;
            }
            $rejected[] = $this->makeException($decoded, $vk_request, $access_token);
        }
        $parallel_results->setResults($fulfilled);
        $parallel_results->setExceptions($rejected);
        return $parallel_results;
    }

    private function isValidResponse(?array $decoded): bool
    {
        if ($decoded === null) {
            return false;
        }
        if (!isset($decoded["response"])) {
            return false;
        }
        return true;
    }

    private function isRetrieable(?array $decoded): bool
    {
        if ($decoded === null) {
            return false;
        }
        $error_code = $decoded['error']['error_code'] ?? null;
        if (!$error_code) {
            return false;
        }
        if (!isset($this->errors_to_retry[$error_code])) {
            return false;
        }
        return true;
    }

    private function makeException(?array $decoded, VkRequestInterface $vk_request, string $access_token): RequestFailedException
    {
        if ($decoded === null) {
            $exception = new RequestFailedException('Failed to decode vk response');
            $exception->setRequest($vk_request);
            $exception->setToken($access_token);
            return $exception;
        }
        $error_msg = $decoded['error']['error_msg'] ?? 'Unknown VK error';
        $exception = new RequestFailedException($error_msg);
        $exception->setRequest($vk_request);
        $exception->setToken($access_token);
        return $exception;
    }

}
