<?php
namespace Multisanti\Vk\Senders\Decorators\Parallel;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\ParallelVkRequest;
use Multisanti\Vk\Requests\ParallelVkRequestInterface;
use Multisanti\Vk\Results\ParallelResult;
use Multisanti\Vk\Results\ParallelResultsCollectionInterface;
use Multisanti\Vk\Senders\ParallelRequestsSenderInterface;
use Multisanti\Vk\Senders\RequestsSenderInterface;

class RetriesHttp implements ParallelRequestsSenderInterface
{
    private $sender;
    private $retrier;

    public function __construct(ParallelRequestsSenderInterface $sender, RequestsSenderInterface $retrier)
    {
        $this->sender = $sender;
        $this->retrier = $retrier;
    }

    /**
     * @param ParallelVkRequestInterface $requests[]
     * @return ParallelResultsCollectionInterface
     */
    public function send(array $requests): ParallelResultsCollectionInterface
    {
        $parallel_results = $this->sender->send($requests);
        $rejected = [];

        $exceptions = $parallel_results->getExceptions();
        while (count($exceptions) > 0) {
            /** @var RequestFailedException $exception */
            $exception = array_pop($exceptions);
            $vk_request = $exception->getRequest();
            $access_token = $exception->getToken();
            try {
                $response = $this->retrier->send($vk_request, $access_token);
                $parallel_request = new ParallelVkRequest($vk_request, $access_token);
                $parallel_result = new ParallelResult($parallel_request, $response);
                $parallel_results->addResult($parallel_result);
            }
            catch (RequestFailedException $e) {
                $rejected[] = $e;
            }
        }
        $parallel_results->setExceptions($rejected);
        return $parallel_results;
    }
}