<?php

namespace Multisanti\Vk\Senders\Decorators\Single;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\VkRequestInterface;
use Multisanti\Vk\Senders\RequestsSenderInterface;

class RetriesVkErrors extends AbstractRetriesDecorator implements RequestsSenderInterface
{
    private $errors_to_retry = [
        6 => 'Too many requests per second',
        9 => 'Flood control',
        10 => 'Internal server error',
    ];

    public function send(VkRequestInterface $vk_request, string $access_token): string
    {
        for ($i = 0; $i < $this->retries; $i++) {
            $response = $this->requests_sender->send($vk_request, $access_token);
            /** Try to decode response to associative array */
            $decoded = json_decode($response, true);
            if ($decoded === null) {
                $exception = new RequestFailedException('Failed to decode vk response');
                $exception->setRequest($vk_request);
                $exception->setToken($access_token);
                throw $exception;
            }
            /** If decoded array has 'response' key return response */
            $result = $decoded['response'] ?? null;
            if ($result !== null) {
                return $response;
            }
            /**
             * If error can be fixed by retry then delay and continue
             * else throw exception with vk error msg
             */
            $error_code = $decoded['error']['error_code'] ?? null;
            $error_msg = $decoded['error']['error_msg'] ?? '';
            if ($error_code !== null) {
                $error_code = intval($error_code);
                if (isset($this->errors_to_retry[$error_code])) {
                    usleep($this->delay * 1000);
                    continue;
                }
                $exception = new RequestFailedException($error_msg);
                $exception->setRequest($vk_request);
                $exception->setToken($access_token);
                throw $exception;
            }
        }
        $exception = new RequestFailedException('VK retries limit exceeded');
        $exception->setRequest($vk_request);
        $exception->setToken($access_token);
        throw $exception;
    }
}
