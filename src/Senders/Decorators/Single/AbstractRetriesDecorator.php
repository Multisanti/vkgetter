<?php

namespace Multisanti\Vk\Senders\Decorators\Single;

use Multisanti\Vk\Requests\VkRequestInterface;
use Multisanti\Vk\Senders\RequestsSenderInterface;

abstract class AbstractRetriesDecorator
{
    protected $requests_sender;
    protected $retries = 3;
    protected $delay = 1500;

    /**
     * @param RequestsSenderInterface $requests_sender
     * @param int $retries
     * @param int $delay In milliseconds (0.001 of second)
     */
    public function __construct(RequestsSenderInterface $requests_sender, int $retries = 3, int $delay = 1500)
    {
        $this->requests_sender = $requests_sender;
        $this->setRetries($retries);
        $this->setDelay($delay);
    }

    abstract public function send(VkRequestInterface $vk_request, string $access_token): string;

    /**
     * @return int
     */
    public function getRetries(): int
    {
        return $this->retries;
    }

    /**
     * @param int $retries
     */
    public function setRetries(int $retries): void
    {
        $this->retries = $retries;
    }

    /**
     * @return int
     */
    public function getDelay(): int
    {
        return $this->delay;
    }

    /**
     * @param int $delay
     */
    public function setDelay(int $delay): void
    {
        $this->delay = $delay;
    }
}
