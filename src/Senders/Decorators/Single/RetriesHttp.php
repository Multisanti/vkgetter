<?php

namespace Multisanti\Vk\Senders\Decorators\Single;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\VkRequestInterface;
use Multisanti\Vk\Senders\RequestsSenderInterface;

class RetriesHttp extends AbstractRetriesDecorator implements RequestsSenderInterface
{
    public function send(VkRequestInterface $vk_request, string $access_token): string
    {
        for ($i = 0; $i < $this->retries; $i++) {
            try {
                $response = $this->requests_sender->send($vk_request, $access_token);
                return $response;
            }
            catch (RequestFailedException $exception) {
                usleep($this->delay * 1000);
                continue;
            }
        }
        $exception = new RequestFailedException("HTTP retries limit exceeded");
        $exception->setRequest($vk_request);
        $exception->setToken($access_token);
        throw $exception;
    }
}
