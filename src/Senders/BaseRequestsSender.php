<?php

namespace Multisanti\Vk\Senders;

use Multisanti\Vk\ApiClients\ApiClientInterface;
use Multisanti\Vk\Exceptions\ApiClientFailedException;
use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\VkRequestInterface;
use Multisanti\Vk\Senders\Options\ApiOptionsInterface;
use Multisanti\Vk\Senders\Options\ApiUrlInterface;

class BaseRequestsSender implements RequestsSenderInterface
{
    protected $client;
    protected $api_url;
    protected $options;

    public function __construct(ApiClientInterface $client, ApiUrlInterface $api_url, ApiOptionsInterface $options)
    {
        $this->client = $client;
        $this->api_url = $api_url;
        $this->options = $options;
    }

    /**
     * @param VkRequestInterface $vk_request
     * @param string $access_token
     * @return string
     * @throws RequestFailedException
     */
    public function send(VkRequestInterface $vk_request, string $access_token): string
    {
        $url = $this->api_url->getMethodUrl($vk_request->getMethodName());
        $params = $this->options->merge($vk_request->getMethodParams(), $access_token);
        try {
            $response = $this->client->post($url, $params);
        }
        catch (ApiClientFailedException $exception) {
            throw $this->wrapException($exception, $vk_request, $access_token);
        }
        return $response;
    }

    private function wrapException(
        \Throwable $exception,
        VkRequestInterface $vk_request,
        string $access_token
    ): RequestFailedException {
        $message = $exception->getMessage();
        $code = $exception->getCode();
        $previous = $exception->getPrevious();
        $wrapped_exception = new RequestFailedException($message, $code, $previous);
        $wrapped_exception->setRequest($vk_request);
        $wrapped_exception->setToken($access_token);
        return $wrapped_exception;
    }
}