<?php

namespace Multisanti\Vk\ApiClients;

use Multisanti\Vk\Exceptions\ApiClientFailedException;

interface ApiClientInterface
{
    /**
     * @param string $url
     * @param array $params
     * @throws ApiClientFailedException
     * @return string
     */
    public function post(string $url, array $params): string;
}