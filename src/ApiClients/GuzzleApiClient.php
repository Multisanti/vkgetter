<?php

namespace Multisanti\Vk\ApiClients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use Multisanti\Vk\Exceptions\ApiClientFailedException;

class GuzzleApiClient implements ApiClientInterface
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function post(string $url, array $params): string
    {
        $form_params = [
            'form_params' => $params,
        ];
        try {
            $response = $this->client->post($url, $form_params);
        }
        catch (ServerException $e) {
            throw $this->wrapException($e, $url, $params);
        }
        catch (TooManyRedirectsException $e) {
            throw $this->wrapException($e, $url, $params);
        }
        catch (ClientException $e) {
            throw $this->wrapException($e, $url, $params);
        }
        catch (ConnectException $e) {
            throw $this->wrapException($e, $url, $params);
        }
        catch (RequestException $e) {
            throw $this->wrapException($e, $url, $params);
        }
        $contents = $response->getBody()->getContents();
        return $contents;
    }

    private function wrapException(\Throwable $exception, string $url, array $params): ApiClientFailedException
    {
        $message = $exception->getMessage();
        $code = $exception->getCode();
        $previous = $exception->getPrevious();
        $wrapped_exception = new ApiClientFailedException($message, $code, $previous);
        $wrapped_exception->setUrl($url);
        $wrapped_exception->setParams($params);
        return $wrapped_exception;
    }

}