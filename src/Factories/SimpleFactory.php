<?php

namespace Multisanti\Vk\Factories;


use GuzzleHttp\Client;
use Multisanti\Vk\ApiClients\GuzzleApiClient;
use Multisanti\Vk\Senders\Options\ApiOptions;
use Multisanti\Vk\Senders\Options\ApiUrl;
use Multisanti\Vk\Senders\ParallelRequestsSenderInterface;
use Multisanti\Vk\Senders\RequestsSenderInterface;

class SimpleFactory
{
    public function makeSingleSender(): RequestsSenderInterface
    {
        $guzzle = new Client();
        $api_client = new GuzzleApiClient($guzzle);
        $api_url = new ApiUrl();
        $api_options = new ApiOptions();
        $factory = new SendersFactory($api_client, $api_url, $api_options);
        return $factory->make(SendersFactory::TYPE_DECORATED);
    }

    public function makeParallelSender(): ParallelRequestsSenderInterface
    {
        $factory = new ParallelSendersFactory();
        return $factory->make(ParallelSendersFactory::TYPE_DECORATED);
    }
}