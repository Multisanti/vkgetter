<?php

namespace Multisanti\Vk\Factories;

use Multisanti\Vk\ApiClients\ApiClientInterface;
use Multisanti\Vk\Senders\BaseRequestsSender;
use Multisanti\Vk\Senders\Decorators\Single\RetriesHttp;
use Multisanti\Vk\Senders\Decorators\Single\RetriesVkErrors;
use Multisanti\Vk\Senders\Options\ApiOptionsInterface;
use Multisanti\Vk\Senders\Options\ApiUrlInterface;
use Multisanti\Vk\Senders\RequestsSenderInterface;

class SendersFactory implements SendersFactoryInterface
{
    public const TYPE_BASE = "base";
    public const TYPE_DECORATED = "decorated";

    private $client;
    private $api_url;
    private $api_options;

    public function __construct(ApiClientInterface $client, ApiUrlInterface $api_url, ApiOptionsInterface $api_options)
    {
        $this->client = $client;
        $this->api_url = $api_url;
        $this->api_options = $api_options;
    }

    public function make(string $type = null): RequestsSenderInterface
    {
        $sender = $this->getDefaultSender();
        if ($type === self::TYPE_DECORATED) {
            $sender = $this->getDecoratedSender($sender);
        }
        return $sender;
    }

    protected function getDefaultSender(): RequestsSenderInterface
    {
        $sender = new BaseRequestsSender($this->client, $this->api_url, $this->api_options);
        return $sender;
    }

    protected function getDecoratedSender(RequestsSenderInterface $sender): RequestsSenderInterface
    {
        $http_decorator = new RetriesHttp($sender);
        $vk_errors_decorator = new RetriesVkErrors($http_decorator);
        return $vk_errors_decorator;
    }
}
