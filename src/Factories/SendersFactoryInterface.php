<?php

namespace Multisanti\Vk\Factories;

use Multisanti\Vk\Senders\RequestsSenderInterface;

interface SendersFactoryInterface
{
    public function make(string $type = null): RequestsSenderInterface;
}