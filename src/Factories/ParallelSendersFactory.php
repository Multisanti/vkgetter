<?php

namespace Multisanti\Vk\Factories;

use GuzzleHttp\Client;
use Multisanti\Vk\ApiClients\ApiClientInterface;
use Multisanti\Vk\ApiClients\GuzzleApiClient;
use Multisanti\Vk\Senders\Decorators\Parallel\RetriesHttp;
use Multisanti\Vk\Senders\Decorators\Parallel\RetriesVkErrors;
use Multisanti\Vk\Senders\GuzzleParallelRequestsSender;
use Multisanti\Vk\Senders\Options\ApiOptions;
use Multisanti\Vk\Senders\Options\ApiOptionsInterface;
use Multisanti\Vk\Senders\Options\ApiUrl;
use Multisanti\Vk\Senders\Options\ApiUrlInterface;
use Multisanti\Vk\Senders\ParallelRequestsSenderInterface;

class ParallelSendersFactory implements ParalleSendersFactoryInterface
{
    public const TYPE_BASE = "base";
    public const TYPE_DECORATED = "decorated";

    public function make(string $type = null): ParallelRequestsSenderInterface
    {
        $guzzle = new Client();
        $api_url = new ApiUrl();
        $api_options = new ApiOptions();

        $sender = new GuzzleParallelRequestsSender($guzzle, $api_url, $api_options);

        if ($type === self::TYPE_DECORATED) {
            $guzzle_client = new GuzzleApiClient($guzzle);
            $factory = new SendersFactory($guzzle_client, $api_url, $api_options);
            $retrier = $factory->make(SendersFactory::TYPE_DECORATED);

            $http_errors = new RetriesHttp($sender, $retrier);
            $vk_errors = new RetriesVkErrors($http_errors, $retrier);
            return $vk_errors;
        }
        return $sender;
    }
}
