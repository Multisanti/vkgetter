<?php

namespace Multisanti\Vk\Factories;

use Multisanti\Vk\Senders\ParallelRequestsSenderInterface;

interface ParalleSendersFactoryInterface
{
    public function make(string $type = null): ParallelRequestsSenderInterface;
}