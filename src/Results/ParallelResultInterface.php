<?php

namespace Multisanti\Vk\Results;

use Multisanti\Vk\Requests\ParallelVkRequestInterface;

interface ParallelResultInterface
{
    public function getRequest(): ParallelVkRequestInterface;
    public function getResult(): string;
}