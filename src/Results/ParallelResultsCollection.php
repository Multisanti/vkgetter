<?php

namespace Multisanti\Vk\Results;

use Multisanti\Vk\Exceptions\RequestFailedException;

class ParallelResultsCollection implements ParallelResultsCollectionInterface
{
    private $results = [];
    private $exceptions = [];

    public function addResult(ParallelResultInterface $result)
    {
        $this->results[] = $result;
    }

    public function addException(RequestFailedException $exception)
    {
        $this->exceptions[] = $exception;
    }
    /**
     * @return array
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @return array
     */
    public function getExceptions(): array
    {
        return $this->exceptions;
    }

    public function setResults(array $results)
    {
        $this->results = [];
        /** @var ParallelResultInterface $parallel_result */
        foreach ($results as $parallel_result) {
            $this->addResult($parallel_result);
        }
    }

    public function setExceptions(array $exceptions)
    {
        $this->exceptions = [];
        /** @var RequestFailedException $exception */
        foreach ($exceptions as $exception) {
            $this->addException($exception);
        }
    }


}
