<?php

namespace Multisanti\Vk\Results;

use Multisanti\Vk\Requests\ParallelVkRequestInterface;

class ParallelResult implements ParallelResultInterface
{
    private $request;
    private $result;

    public function __construct(ParallelVkRequestInterface $request, string $result)
    {
        $this->request = $request;
        $this->result = $result;
    }

    public function getRequest(): ParallelVkRequestInterface
    {
        return $this->request;
    }

    public function getResult(): string
    {
        return $this->result;
    }

}