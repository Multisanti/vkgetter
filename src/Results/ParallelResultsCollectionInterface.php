<?php
namespace Multisanti\Vk\Results;

use Multisanti\Vk\Exceptions\RequestFailedException;

interface ParallelResultsCollectionInterface
{
    public function addResult(ParallelResultInterface $result);
    public function addException(RequestFailedException $exception);

    public function setResults(array $results);
    public function setExceptions(array $exceptions);
    /**
     * @return ParallelResult[]
     */
    public function getResults(): array;
    public function getExceptions(): array;
}
