<?php

namespace Multisanti\Vk\Requests;

class ExecuteVkRequest extends AbstractVkRequest implements VkRequestInterface
{
    public function __construct(array $method_params)
    {
        parent::__construct("execute", $method_params);
    }

    /**
     * @param array ExecutableInterface[]
     */
    protected function setMethodParams(array $params)
    {
        foreach ($params as $param) {
            if (!($param instanceof ExecutableInterface)) {
                throw new \InvalidArgumentException("ExecuteVkRequest accepts only array of ExecutableInterface implementations");
            }
        }
        $this->method_params = $params;
    }
    public function getMethodParams(): array
    {
        $methods_list = [];
        /** @var ExecutableInterface $method */
        foreach ($this->method_params as $method) {
            $methods_list[] = $method->getAsExecute();
        }
        $methods_list_as_string = implode(",", $methods_list);
        $code = "return [{$methods_list_as_string}];";
        return ["code" => $code];
    }
}