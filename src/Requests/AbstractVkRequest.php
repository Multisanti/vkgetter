<?php

namespace Multisanti\Vk\Requests;

abstract class AbstractVkRequest
{
    protected $method_name;
    protected $method_params;

    public function __construct(string $method_name, array $method_params)
    {
        $this->setMethodName($method_name);
        $this->setMethodParams($method_params);
    }

    protected function setMethodName(string $name)
    {
        $this->method_name = $name;
    }

    public function getMethodName(): string
    {
        return $this->method_name;
    }

    abstract protected function setMethodParams(array $params);
    abstract public function getMethodParams(): array;
}