<?php

namespace Multisanti\Vk\Requests;

interface ParallelVkRequestInterface
{
    public function getRequest(): VkRequestInterface;
    public function getToken(): string;
}