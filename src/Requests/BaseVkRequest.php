<?php

namespace Multisanti\Vk\Requests;

class BaseVkRequest extends AbstractVkRequest implements VkRequestInterface, ExecutableInterface
{
    protected function setMethodParams(array $params)
    {
        $method_params = [];
        // encode array as comma separated string
        foreach ($params as $key => $value) {
            if (is_array($value)) {
                $value = implode(",", $value);
            }
            $method_params[$key] = $value;
        }
        $this->method_params = $method_params;
    }

    public function getMethodParams(): array
    {
        return $this->method_params;
    }

    public function getAsExecute(): string
    {
        $name = $this->getMethodName();
        $params = $this->getMethodParams();
        $params_encoded = json_encode($params);
        return "API.{$name}({$params_encoded})";
    }


}