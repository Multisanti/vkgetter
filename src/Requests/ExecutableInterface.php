<?php

namespace Multisanti\Vk\Requests;

interface ExecutableInterface
{
    public function getAsExecute(): string;
}