<?php

namespace Multisanti\Vk\Requests;

class ParallelVkRequest implements ParallelVkRequestInterface
{
    private $request;
    private $access_token;

    public function __construct(VkRequestInterface $request, string $access_token)
    {
        $this->request = $request;
        $this->access_token = $access_token;
    }

    public function getRequest(): VkRequestInterface
    {
        return $this->request;
    }

    public function getToken(): string
    {
        return $this->access_token;
    }

}