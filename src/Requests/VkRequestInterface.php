<?php
namespace Multisanti\Vk\Requests;

interface VkRequestInterface
{
    public function getMethodName(): string;
    public function getMethodParams(): array;
}