### Для чего эта библиотека

- Отправка запросов к VK API
- Поддержка одиночных запросов и параллельных
- Повтор запросов в случае HTTP ошибок или ошибок VK
- В случае ошибки, возвращается исключение с названием метода, параметрами, токеном и описанием ошибки
- Генерация execute запросов

### Базовое использование
1. Создать клиент через SimpleFactory (одиночный или параллельный)
Одиночный отправщик запросов:
```php
		<?php
		$factory = new \Multisanti\Vk\Factories\SimpleFactory();
		$sender = $factory->makeSingleSender();
		?>
```
Параллельный отправщик запросов:
```php
		<?php
		$factory = new \Multisanti\Vk\Factories\SimpleFactory();
		$sender = $factory->makeParallelSender();
		?>
```
2. Отправка одиночного запроса
```php
		$params = [
        	"user_ids" => [1,2,3],
		];
		$request = new \Multisanti\Vk\Requests\BaseVkRequest("users.get", $params);
		try {
			$result = $sender->send($request, $access_token);
		}
		catch (\Multisanti\Vk\Exceptions\RequestFailedException $exception) {
			$request = $exception->getRequest();
			$token = $exception->getToken();
		}
```
3. Отправка execute
```php
		$requests = [];
		$step = 300;
		for ($i = 0; $i < 10; $i++) {
			$params = [
				"user_ids" => range(($i * $step), ($i + 1) * $step)
			];
			$request = new \Multisanti\Vk\Requests\BaseVkRequest("users.get", $params);
			$requests[] = $request;
		}
		$execute = new \Multisanti\Vk\Requests\ExecuteVkRequest($requests);
		try {
			$result = $sender->send($execute, $access_token);
		}
		catch (\Multisanti\Vk\Exceptions\RequestFailedException $exception) {
			$request = $exception->getRequest();
			$token = $exception->getToken();
		}
```
4. Отправка параллельных запросов
```php
		$requests = [];
		$step = 300;
		for ($i = 0; $i < 10; $i++) {
			$params = [
				"user_ids" => range(($i * $step), ($i + 1) * $step)
			];
			$request = new \Multisanti\Vk\Requests\BaseVkRequest("users.get", $params);
			$parallel_request = new \Multisanti\Vk\Requests\ParallelVkRequest($request, $access_token);
			$requests[] = $parallel_request;
		}
		$results = $sender->send($requests);
		$fulfilled = $results->getResults();
		$exceptions = $results->getExceptions();
		/** @var \Multisanti\Vk\Results\ParallelResultInterface $parallel_result */
		foreach ($fulfilled as $parallel_result) {
			echo $parallel_result->getResult();
			/** @var \Multisanti\Vk\Requests\ParallelVkRequestInterface $request */
			$request = $parallel_result->getRequest();
			$method = $request->getRequest()->getMethodName();
			$params = $request->getRequest()->getMethodParams();
			$token = $request->getToken();
		}
		/** @var \Multisanti\Vk\Exceptions\RequestFailedException $exception */
		foreach ($exceptions as $exception) {
			echo $exception->getMessage();
			$request = $exception->getRequest();
			$token = $exception->getToken();
		}
```
