<?php

namespace Multisanti\Vk\Senders\Options;

use PHPUnit\Framework\TestCase;

class ApiOptionsTest extends TestCase
{
    public function test_get()
    {
        $instance = new ApiOptions('5.0', 'en');
        $result = $instance->get();
        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertArrayHasKey('v', $result);
        $this->assertEquals('5.0', $result['v']);
        $this->assertArrayHasKey('lang', $result);
        $this->assertEquals('en', $result['lang']);
    }

    public function test_merge()
    {
        $instance = new ApiOptions('5.0', 'en');
        $method_params = [
            "value_1" => 1,
            "value_2" => 2,
        ];
        $access_token = "somestring";
        $merged = $instance->merge($method_params, $access_token);
        $expected = [
            "value_1" => 1,
            "value_2" => 2,
            "v" => '5.0',
            "lang" => 'en',
            'access_token' => $access_token,
        ];
        $this->assertEquals($expected, $merged);
    }
}
