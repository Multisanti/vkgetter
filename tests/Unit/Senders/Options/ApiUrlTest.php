<?php

namespace Multisanti\Vk\Senders\Options;

use PHPUnit\Framework\TestCase;

class ApiUrlTest extends TestCase
{
    public function testGetMethodUrl()
    {
        $base = 'base';
        $method = 'method';
        $instance = new ApiUrl($base);
        $actual = $instance->getMethodUrl($method);
        $expected = "{$base}/$method";
        $this->assertEquals($expected, $actual);
    }
}
