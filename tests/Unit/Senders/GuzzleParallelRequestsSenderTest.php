<?php

namespace Multisanti\Vk\Senders;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\BaseVkRequest;
use Multisanti\Vk\Requests\ParallelVkRequest;
use Multisanti\Vk\Requests\VkRequestInterface;
use Multisanti\Vk\Results\ParallelResult;
use Multisanti\Vk\Results\ParallelResultInterface;
use Multisanti\Vk\Results\ParallelResultsCollectionInterface;
use Multisanti\Vk\Senders\Options\ApiOptions;
use Multisanti\Vk\Senders\Options\ApiUrl;
use PHPUnit\Framework\TestCase;

class GuzzleParallelRequestsSenderTest extends TestCase
{

    public function test_send()
    {
        $mock = new MockHandler([
            new Response(200, [], "test response"),
            new Response(200, [], "test response"),
            new RequestException("Error Communicating with Server", new Request('GET', 'test'))
        ]);
        $handler = HandlerStack::create($mock);
        $client = new Client(['handler' => $handler]);
        
        $api_url = new ApiUrl();
        $api_options = new ApiOptions();
        $instance = new GuzzleParallelRequestsSender($client, $api_url, $api_options);
        $access_token = "test token";
        $parallel_requests = [
            new ParallelVkRequest(new BaseVkRequest("users.get", ["user_id" => 1]), $access_token),
            new ParallelVkRequest(new BaseVkRequest("users.get", ["user_id" => 1]), $access_token),
            new ParallelVkRequest(new BaseVkRequest("users.get", ["user_id" => 1]), $access_token),
        ];
        $result = $instance->send($parallel_requests);
        $this->assertInstanceOf(ParallelResultsCollectionInterface::class, $result);
        $results = $result->getResults();
        $exceptions = $result->getExceptions();
        $this->assertIsArray($results);
        $this->assertIsArray($exceptions);
        $this->assertCount(2, $results);
        /** @var ParallelResult $first_result */
        $first_result = $results[0];
        $this->assertInstanceOf(ParallelResultInterface::class, $first_result);
        $this->assertEquals("test response", $first_result->getResult());
        $this->assertCount(1, $exceptions);
        /** @var RequestFailedException $exception */
        $exception = $exceptions[0];
        $this->assertInstanceOf(RequestFailedException::class, $exception);
        $this->assertEquals("Error Communicating with Server", $exception->getMessage());
        $this->assertInstanceOf(VkRequestInterface::class, $exception->getRequest());
        $this->assertEquals("test token", $exception->getToken());
    }
}
