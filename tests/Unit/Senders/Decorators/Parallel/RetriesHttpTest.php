<?php

namespace Multisanti\Vk\Senders\Decorators\Parallel;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\BaseVkRequest;
use Multisanti\Vk\Requests\ParallelVkRequest;
use Multisanti\Vk\Results\ParallelResult;
use Multisanti\Vk\Results\ParallelResultInterface;
use Multisanti\Vk\Results\ParallelResultsCollection;
use Multisanti\Vk\Results\ParallelResultsCollectionInterface;
use Multisanti\Vk\Senders\BaseRequestsSender;
use Multisanti\Vk\Senders\GuzzleParallelRequestsSender;
use PHPUnit\Framework\TestCase;

class RetriesHttpTest extends TestCase
{

    public function tearDown(): void
    {
        \Mockery::close();
    }

    public function test_send()
    {
        $vk_request = new BaseVkRequest("users.get", ["user_ids" => 1,2]);
        $access_token = "test_token";
        $requests = [
            new ParallelVkRequest($vk_request, $access_token),
            new ParallelVkRequest($vk_request, $access_token),
        ];

        $sender_return = new ParallelResultsCollection();
        $sender_return->addResult(new ParallelResult($requests[0], "result 1"));
        $sender_return->addResult(new ParallelResult($requests[1], "result 2"));

        $sender = \Mockery::mock(GuzzleParallelRequestsSender::class);
        $sender->allows()->send()->with($requests)->once()->andReturn($sender_return);
        $retrier = \Mockery::mock(BaseRequestsSender::class);
        $retrier->shouldNotReceive('send');
        $instance = new RetriesHttp($sender, $retrier);
        $instance_result = $instance->send($requests);
        $this->assertInstanceOf(ParallelResultsCollectionInterface::class, $instance_result);
        $this->assertNotEmpty($instance_result->getResults());
        $this->assertCount(2, $instance_result->getResults());
        $this->assertInstanceOf(ParallelResultInterface::class, $instance_result->getResults()[0]);
        $this->assertEquals("result 1", $instance_result->getResults()[0]->getResult());
    }

    public function test_send_withException()
    {
        $vk_request = new BaseVkRequest("users.get", ["user_ids" => 1,2]);
        $access_token = "test_token";
        $requests = [
            new ParallelVkRequest($vk_request, $access_token),
            new ParallelVkRequest($vk_request, $access_token),
        ];

        $sender_return = new ParallelResultsCollection();
        $sender_return->addResult(new ParallelResult($requests[0], "result 1"));
        $exception = new RequestFailedException("test exception");
        $exception->setRequest($vk_request);
        $exception->setToken($access_token);
        $sender_return->addException($exception);

        $sender = \Mockery::mock(GuzzleParallelRequestsSender::class);
        $sender->allows()->send()->with($requests)->once()->andReturn($sender_return);

        $retrier = \Mockery::mock(BaseRequestsSender::class);
        $exception_retrier = new RequestFailedException("retries exception");
        $retrier->allows()->send($vk_request, $access_token)->once()->andThrow($exception_retrier);

        $instance = new RetriesHttp($sender, $retrier);
        $instance_result = $instance->send($requests);
        $this->assertIsArray($instance_result->getResults());
        $this->assertIsArray($instance_result->getExceptions());
        $this->assertCount(1, $instance_result->getResults());
        $this->assertCount(1, $instance_result->getExceptions());
        $this->assertInstanceOf(RequestFailedException::class, $instance_result->getExceptions()[0]);
        $this->assertEquals("retries exception", $instance_result->getExceptions()[0]->getMessage());
    }
}
