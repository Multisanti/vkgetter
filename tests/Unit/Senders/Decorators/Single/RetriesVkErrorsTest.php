<?php

namespace Multisanti\Vk\Senders\Decorators\Single;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\BaseVkRequest;
use Multisanti\Vk\Senders\BaseRequestsSender;
use PHPUnit\Framework\TestCase;

class RetriesVkErrorsTest extends TestCase
{
    private $requests_sender;
    private $vk_request;
    private $access_token;

    public function setUp(): void
    {
        $this->requests_sender = \Mockery::mock(BaseRequestsSender::class);
        $method_name = 'users.get';
        $method_params = ['user_ids' => [1,2,3]];
        $this->vk_request = new BaseVkRequest($method_name, $method_params);
        $this->access_token = "abcd";
    }

    public function test_send()
    {
        $expected_response = json_encode([
            'response' => [
                'items' => [1,2,3],
            ]
        ]);

        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andReturn($expected_response);

        $instance = new RetriesVkErrors($this->requests_sender);
        $result = $instance->send($this->vk_request, $this->access_token);
        $this->assertEquals($expected_response, $result);
    }

    public function test_send_emptyResponse()
    {
        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andReturn('')
            ->once();
        $instance = new RetriesVkErrors($this->requests_sender);
        $this->expectException(RequestFailedException::class);
        $result = $instance->send($this->vk_request, $this->access_token);
    }
    public function test_send_errorNoRetry()
    {
        $response = json_encode([
            'error' => [
                'error_code' => 1,
                'error_msg' => 'some error',
            ]
        ]);
        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andReturn($response);
        $instance = new RetriesVkErrors($this->requests_sender);
        $this->expectException(RequestFailedException::class);
        $result = $instance->send($this->vk_request, $this->access_token);
    }

    public function test_send_errorNoRetryExceptionContent()
    {
        $response = json_encode([
            'error' => [
                'error_code' => 1,
                'error_msg' => 'some error',
            ]
        ]);
        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andReturn($response);
        $instance = new RetriesVkErrors($this->requests_sender);
        try {
            $result = $instance->send($this->vk_request, $this->access_token);
        }
        catch (RequestFailedException $exception) {
            $this->assertEquals($this->vk_request, $exception->getRequest());
            $this->assertEquals($this->access_token, $exception->getToken());
            $this->assertEquals('some error', $exception->getMessage());
        }
    }

    public function test_send_errorRetry()
    {
        $retries = 3;
        $response = json_encode([
            'error' => [
                'error_code' => 6,
                'error_msg' => 'some error',
            ]
        ]);
        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andReturn($response)
            ->times($retries);
        $instance = new RetriesVkErrors($this->requests_sender);
        $instance->setRetries($retries);
        $instance->setDelay(0);
        $this->expectException(RequestFailedException::class);
        $result = $instance->send($this->vk_request, $this->access_token);
    }

    public function test_send_receivedException()
    {
        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andThrow(new RequestFailedException('some exception'));
        $instance = new RetriesVkErrors($this->requests_sender);
        $this->expectException(RequestFailedException::class);
        $this->expectExceptionMessage('some exception');
        $result = $instance->send($this->vk_request, $this->access_token);
    }
}
