<?php

namespace Multisanti\Vk\Senders\Decorators\Single;

use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\BaseVkRequest;
use Multisanti\Vk\Senders\BaseRequestsSender;
use PHPUnit\Framework\TestCase;

class RetriesHttpTest extends TestCase
{
    private $requests_sender;
    private $vk_request;
    private $access_token;

    public function setUp(): void
    {
        $this->requests_sender = \Mockery::mock(BaseRequestsSender::class);
        $method_name = 'users.get';
        $method_params = ['user_ids' => [1,2,3]];
        $this->vk_request = new BaseVkRequest($method_name, $method_params);
        $this->access_token = "abcd";
    }

    public function tearDown(): void
    {
        \Mockery::close();
    }

    public function test_send()
    {
        $expected_response = json_encode([
            'response' => [
                'items' => [1,2,3],
            ]
        ]);

        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andReturn($expected_response);
        $instance = new RetriesHttp($this->requests_sender);
        $result = $instance->send($this->vk_request, $this->access_token);
        $this->assertEquals($expected_response, $result);
    }

    /**
     * Should throw RequestFailedException after 3 retries
     * @throws RequestFailedException
     */
    public function test_send_exceptionThrow()
    {
        $expected_exception = new RequestFailedException("request failed");
        $expected_exception->setRequest($this->vk_request);
        $expected_exception->setToken($this->access_token);

        $retries = 3;
        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andThrow($expected_exception)
            ->times($retries);
        $instance = new RetriesHttp($this->requests_sender);
        $instance->setRetries($retries);
        $instance->setDelay(0);
        $this->expectException(RequestFailedException::class);
        $instance->send($this->vk_request, $this->access_token);
    }

    public function test_send_exceptionContent()
    {
        $expected_exception = new RequestFailedException("request failed");
        $expected_exception->setRequest($this->vk_request);
        $expected_exception->setToken($this->access_token);

        $retries = 1;
        $this->requests_sender
            ->allows()
            ->send()
            ->withAnyArgs()
            ->andThrow($expected_exception)
            ->times($retries);
        $instance = new RetriesHttp($this->requests_sender);
        $instance->setRetries($retries);
        $instance->setDelay(0);
        try {
            $instance->send($this->vk_request, $this->access_token);
        }
        catch (RequestFailedException $exception) {
            $this->assertEquals($this->vk_request, $exception->getRequest());
            $this->assertEquals($this->access_token, $exception->getToken());
        }
    }
}
