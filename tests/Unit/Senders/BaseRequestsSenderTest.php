<?php

namespace Multisanti\Vk\Senders;

use Multisanti\Vk\ApiClients\GuzzleApiClient;
use Multisanti\Vk\Exceptions\ApiClientFailedException;
use Multisanti\Vk\Exceptions\RequestFailedException;
use Multisanti\Vk\Requests\BaseVkRequest;
use Multisanti\Vk\Senders\Options\ApiOptions;
use Multisanti\Vk\Senders\Options\ApiUrl;
use PHPUnit\Framework\TestCase;

class BaseRequestsSenderTest extends TestCase
{
    /** @var ApiUrl */
    private $api_url;
    /** @var ApiOptions */
    private $options;

    private $method_name;
    private $method_params;

    private $vk_request;
    private $access_token;

    private $merged_params;
    private $expected_response;

    private $client;

    public function setUp(): void
    {
        $this->api_url = new ApiUrl();
        $this->options = new ApiOptions();

        $this->method_name = 'users.get';
        $this->method_params = ['user_ids' => [1,2,3]];
        $this->vk_request = new BaseVkRequest($this->method_name, $this->method_params);
        $this->access_token = "abcd";

        $this->merged_params = array_merge(
            $this->vk_request->getMethodParams(),
            $this->options->get(),
            ["access_token" => $this->access_token]
        );
        $this->expected_response = json_encode([
            'response' => [
                'items' => [1,2,3],
            ]
        ]);
        $this->client = \Mockery::mock(GuzzleApiClient::class);
    }

    public function tearDown(): void
    {
        \Mockery::close();
    }

    public function test_send()
    {
        $url = $this->api_url->getMethodUrl($this->method_name);
        $this->client->allows()->post($url, $this->merged_params)->andReturn($this->expected_response)->once();
        $instance = new BaseRequestsSender($this->client, $this->api_url, $this->options);
        $result = $instance->send($this->vk_request, $this->access_token);
        $this->assertEquals($this->expected_response, $result);
    }

    public function test_send_exceptionThrow()
    {
        $url = $this->api_url->getMethodUrl($this->method_name);
        $this->client
            ->allows()
            ->post($url, $this->merged_params)
            ->andThrow(new ApiClientFailedException('api client exception'));
        $instance = new BaseRequestsSender($this->client, $this->api_url, $this->options);
        $this->expectException(RequestFailedException::class);
        $this->expectExceptionMessage('api client exception');
        $result = $instance->send($this->vk_request, $this->access_token);
    }

    public function test_send_exceptionContents()
    {
        $url = $this->api_url->getMethodUrl($this->method_name);
        $this->client
            ->allows()
            ->post($url, $this->merged_params)
            ->andThrow(new ApiClientFailedException('api client exception'));
        $instance = new BaseRequestsSender($this->client, $this->api_url, $this->options);
        try {
            $result = $instance->send($this->vk_request, $this->access_token);
        }
        catch (RequestFailedException $exception) {
            $this->assertEquals($this->vk_request, $exception->getRequest());
            $this->assertEquals($this->access_token, $exception->getToken());
        }
    }
}
