<?php

namespace Multisanti\Vk\Factories;

use Multisanti\Vk\ApiClients\ApiClientInterface;
use Multisanti\Vk\Senders\BaseRequestsSender;
use Multisanti\Vk\Senders\Decorators\Single\RetriesVkErrors;
use Multisanti\Vk\Senders\Options\ApiOptionsInterface;
use Multisanti\Vk\Senders\Options\ApiUrlInterface;
use PHPUnit\Framework\TestCase;

class SendersFactoryTest extends TestCase
{
    public function test_make()
    {
        $client = \Mockery::mock(ApiClientInterface::class);
        $api_url = \Mockery::mock(ApiUrlInterface::class);
        $options = \Mockery::mock(ApiOptionsInterface::class);

        $instance = new SendersFactory($client, $api_url, $options);
        $result = $instance->make();
        $this->assertInstanceOf(BaseRequestsSender::class, $result);
        $result = $instance->make(SendersFactory::TYPE_DECORATED);
        $this->assertInstanceOf(RetriesVkErrors::class, $result);
    }
}
