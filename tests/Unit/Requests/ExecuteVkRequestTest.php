<?php

use Multisanti\Vk\Requests\BaseVkRequest;
use Multisanti\Vk\Requests\ExecuteVkRequest;

class ExecuteVkRequestTest extends \PHPUnit\Framework\TestCase
{

    public function test_getMethodParams()
    {
        $method_params = [
            new BaseVkRequest("users.get", ["user_ids" => [1,2], "fields" => ["screen_name","city"]]),
            new BaseVkRequest("users.get", ["user_ids" => [3,4], "fields" => ["screen_name","city"]]),
            new BaseVkRequest("users.get", ["user_ids" => [5,6], "fields" => ["screen_name","city"]]),
        ];
        $instance = new ExecuteVkRequest($method_params);
        $params = $instance->getMethodParams();
        $this->assertIsArray($params);
        $this->assertArrayHasKey("code", $params);
        $expected = 'return [API.users.get({"user_ids":"1,2","fields":"screen_name,city"}),API.users.get({"user_ids":"3,4","fields":"screen_name,city"}),API.users.get({"user_ids":"5,6","fields":"screen_name,city"})];';
        $this->assertEquals($expected, $params["code"]);
        $this->assertTrue(true);
    }

    public function test_getMethodParams_withWrongParams()
    {
        $this->expectException(\InvalidArgumentException::class);
        $method_params = [
            new BaseVkRequest("users.get", ["user_ids" => [1,2], "fields" => ["screen_name","city"]]),
            new BaseVkRequest("users.get", ["user_ids" => [3,4], "fields" => ["screen_name","city"]]),
            [1,3], // only ExecutableInterface implementations allowed
        ];
        $instance = new ExecuteVkRequest($method_params);
    }
}
