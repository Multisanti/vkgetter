<?php

namespace Multisanti\Vk\Requests;

class BasicVkRequestTest extends \PHPUnit\Framework\TestCase
{
    public function test_setMethodParams_withArrayValues()
    {
        $method_params = [
            'user_ids' => [1, 2, 3],
            'fields' => ["screen_name", "city", "country"],
        ];
        $instance = new BaseVkRequest("users.get", $method_params);
        $params = $instance->getMethodParams();
        $this->assertArrayHasKey("user_ids", $params);
        $this->assertIsNotArray($params["user_ids"]);
        $this->assertEquals("1,2,3", $params["user_ids"]);
    }

    public function test_getAsExecute()
    {
        $method_params = [
            'user_ids' => [1, 2, 3],
            'fields' => "screen_name,city,country",
        ];
        $instance = new BaseVkRequest("users.get", $method_params);
        $expected = 'API.users.get({"user_ids":"1,2,3","fields":"screen_name,city,country"})';
        $this->assertEquals($expected, $instance->getAsExecute());
    }
}
