<?php

namespace Multisanti\Vk\ApiClients;

use PHPUnit\Framework\TestCase;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use Multisanti\Vk\Exceptions\ApiClientFailedException;

class GuzzleApiClientTest extends TestCase
{

    public function test_post_checkUrlAndSentParams()
    {
        $stack = HandlerStack::create();
        /** history */
        $container = [];
        $history = Middleware::history($container);
        $stack->push($history);
        /** client */
        $client = new Client(['handler' => $stack]);

        $api = new GuzzleApiClient($client);
        $url = 'https://api.vk.com/method/users.get';
        $params = ['user_ids' => 1];
        $api->post($url, $params);
        $this->assertCount(1, $container);
        $this->assertIsArray($container[0]);
        $this->assertArrayHasKey('request', $container[0]);
        $this->assertInstanceOf(Request::class, $container[0]['request']);
        /** @var \GuzzleHttp\Psr7\Request $request */
        $request = $container[0]['request'];
        $query = strval($request->getBody());
        $requested_url = strval($request->getUri());
        $method = $request->getMethod();
        $this->assertEquals($url, $requested_url);
        $this->assertEquals(http_build_query($params), $query);
        $this->assertEquals("POST", $method);
    }

    /**
     * @dataProvider exceptionsDataProvider
     */
    public function test_post_exception(\Throwable $exception, $expected)
    {
        $mock = new MockHandler([
            $exception,
        ]);
        $stack = HandlerStack::create($mock);
        $client = new Client(['handler' => $stack]);
        $api = new GuzzleApiClient($client);
        $url = 'https://api.vk.com/method/users.get';
        $params = ['user_ids' => 1];
        $this->expectException(ApiClientFailedException::class);
        $this->expectExceptionMessage($expected);
        $api->post($url, $params);
    }

    public function test_post_response()
    {
        $response_body = json_encode([
            'response' => [
                'items' => [1]
            ]
        ]);
        $mock = new MockHandler([
            new Response(200, [], $response_body),
        ]);
        $stack = HandlerStack::create($mock);
        $client = new Client(['handler' => $stack]);
        $api = new GuzzleApiClient($client);
        $url = 'https://api.vk.com/method/users.get';
        $params = ['user_ids' => 1];
        $result = $api->post($url, $params);
        $this->assertEquals($response_body, $result);
    }

    public function exceptionsDataProvider()
    {
        $sample_request = new Request('GET', 'test');
        return [
            [new ServerException('1', $sample_request), '1'],
            [new TooManyRedirectsException('2', $sample_request), '2'],
            [new ClientException('3', $sample_request), '3'],
            [new ConnectException('4', $sample_request), '4'],
            [new RequestException('5', $sample_request), '5'],
        ];
    }
}

